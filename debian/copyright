Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: iconv
Source: https://github.com/processone/iconv

Files: *
Copyright: 2002-2025 ProcessOne
           2003-2025 Alexey Shchepin <alexey@process-one.net>
           2013-2025 Evgeniy Khramtsov <ekhramtsov@process-one.net>
License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 .
 On Debian systems, the full text of the Apache License version 2.0
 can be found in the file
 `/usr/share/common-licenses/Apache-2.0'.

Files: configure
Copyright: 1992-2012 Free Software Foundation, Inc.
License: FSF
 This configure script is free software; the Free Software Foundation
 gives unlimited permission to copy, distribute and modify it.

Files: debian/*
Copyright: 2014-2025 Philipp Huebner <debalance@debian.org>
License: GPL-2+
 This package is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 02111-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.
